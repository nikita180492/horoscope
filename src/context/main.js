import React, {createContext, useState} from 'react';

export const Main = createContext();

export const MainContextProvider = ({children}) => {

  const [sign, setSign] = useState({});

  return (
    <MainContext.Provider value={{
      sign, setSign,
    }}>
      {children}
    </MainContext.Provider>
  );

};
