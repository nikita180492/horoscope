export const Color = {
  color1: '#030b15',
  color2: '#192d50',
  color3: '#4f6073',
  color4: '#f7f6f7',
  color5: '#afb9c5',
};
