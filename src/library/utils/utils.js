import React from 'react'

import {Dimensions} from 'react-native'
import Leo from '../../assets/images/horoscope-leo.svg'
import Virgo from '../../assets/images/horoscope-virgo.svg'
import Pisces from '../../assets/images/horoscope-pisces.svg'
import Cancer from '../../assets/images/cancer-horoscope.svg'
import Gemini from '../../assets/images/gemini-horoscope.svg'
import Taurus from '../../assets/images/horoscope-taurus.svg'
import Scorpio from '../../assets/images/horoscope-scorpio.svg'
import Aries from '../../assets/images/aries-astrology.svg'
import Aquarius from '../../assets/images/aquarius-astrology.svg'
import Capricorn from '../../assets/images/capricorn-horoscope.svg'
import Sagittarius from '../../assets/images/horoscope-sagitarius.svg'
import Libra from '../../assets/images/horoscope.svg'

import {Color} from './colors'

export const width = Math.round(Dimensions.get('window').width)

export const height = Math.round(Dimensions.get('window').height)

export const fill = Color.color4

export const Signs = [
  {
    id: 1,
    title: 'Aries',
    image: <Aries width={50} height={50}/>
  },
  {
    id: 2,
    title: 'Taurus',
    image: <Taurus width={50} height={50}/>
  },
  {
    id: 3,
    title: 'Gemini',
    image: <Gemini width={50} height={50}/>
  },
  {
    id: 4,
    title: 'Cancer',
    image: <Cancer width={50} height={50}/>
  },
  {
    id: 5,
    title: 'Leo',
    image: <Leo width={50} height={50}/>
  },
  {
    id: 6,
    title: 'Virgo',
    image: <Virgo width={50} height={50}/>
  },
  {
    id: 7,
    title: 'Libra',
    image: <Libra width={50} height={50} />
  },
  {
    id: 8,
    title: 'Scorpio',
    image: <Scorpio width={50} height={50} />
  },
  {
    id: 9,
    title: 'Sagittarius',
    image: <Sagittarius width={50} height={50} />
  },
  {
    id: 10,
    title: 'Capricorn',
    image: <Capricorn width={50} height={50} />
  },
  {
    id: 11,
    title: 'Aquarius',
    image: <Aquarius width={50} height={50} />
  },
  {
    id: 12,
    title: 'Pisces',
    image: <Pisces width={50} height={50} />
  },
]

export const receiveSvg = (title) => {
  let arr = Signs.filter(s => s.title === title)[0]
  return arr.image
}


