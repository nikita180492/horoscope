import {StyleSheet} from 'react-native';
import {width, height} from '../../library/utils/utils';
import {Color} from '../../library/utils/colors';

export default StyleSheet.create({
  ScrollContainer: {
    flexGrow: 1,
    paddingVertical: 30
    // backgroundColor: '#fff',
  },
  test: {
    borderWidth: 1,
    borderColor: 'red',
  },
  Image:{
    width: '100%',
    height: '100%'
  },
  CenterY: {},
  Container: {
    paddingHorizontal: 15,
  },
  LoginBg: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    width: '100%',
    height: '100%',
  },
  Overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0,0,0,0.5 )',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  SignsList:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginHorizontal: -10
  },
  Q:{
    color: Color.color4,
    fontSize: 32,
    lineHeight: 34,
    fontFamily: 'GE Zodiac'
  },
  SignsBlock: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: Color.color5,
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    // height: 70,
    marginHorizontal: 10,
    marginVertical: 10,
    flexGrow: 1,
    flexShrink: 0,
    flexBasis: '33.33 %',
  },
  SignsBlockRipple:{
    width: '100%',
    paddingHorizontal: 10,
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  SignsBlockImage:{
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  SignsBlockText:{
    paddingVertical: 10,
    paddingHorizontal: 10,
    color: Color.color4
  },
  H1: {
    fontFamily: 'SFProDisplay-Regular',
    color: Color.color4,
    fontSize: 32,
    lineHeight: 34,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  H2: {
    fontFamily: 'SFProDisplay-Regular',
    color: Color.color4,
    fontSize: 24,
    lineHeight: 26,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  H3: {
    fontFamily: 'SFProDisplay-Regular',
    color: Color.color4,
    fontSize: 19,
    lineHeight: 21,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  H4: {
    fontFamily: 'SFProDisplay-Regular',
    color: Color.color4,
    fontSize: 16,
    lineHeight: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  H5: {
    fontFamily: 'SFProDisplay-Regular',
    color: Color.color4,
    fontSize: 13,
    lineHeight: 15,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  H6: {
    fontFamily: 'SFProDisplay-Regular',
    color: Color.color4,
    fontSize: 11,
    lineHeight: 13,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  Text: {
    fontFamily: 'SFProDisplay-Regular',
    color: Color.color4,
    // color: 'copper',
    fontSize: 14,
    lineHeight: 18,
    fontWeight: '400',
  },
  Sign: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: Color.color5,
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: 'flex-start',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 90,
    marginBottom: 20
  },
  SignImage: {
    width: 70,
    height: 70,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center'
    // backgroundColor: Color.color2,
  },
  SignInfo: {},
  SignInfoText: {
    paddingVertical: 7,
    marginBottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
    // fontWeight: 'normal'
  },
  SignButtonsArea: {
    justifyContent: 'space-between',
    marginHorizontal: -5,
    flexDirection: 'row'
  },
  SignButton:{
    marginHorizontal: 5,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#024f94',
    flexGrow: 0,
    flexShrink: 1,
    flexBasis: '33.33%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: 20
  },
  SignButtonActive:{
    backgroundColor: "#024f94",
  },
  TextColor:{
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 2,

  },
  SignButtonText:{
    fontFamily: 'SFProDisplay-Regular',
    color: Color.color4,
    fontSize: 19,
    lineHeight: 22,
    fontWeight: '400',
  },
  SignInfoCol: {
    alignItems: 'center',
    flexGrow: 0,
    flexShrink: 1,
    flexBasis: '50%',
  },
  SignDescription:{
    marginVertical: 10
  },
  SignDescriptionText:{
    fontFamily: 'SFProDisplay-Regular',
    paddingVertical: 5,
    paddingHorizontal: 5,
    fontSize: 17,
    lineHeight: 22
  },
  GoToInApp:{
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#024f94',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,

  },
  GoToInAppRipple:{
    paddingHorizontal: 30,
    paddingVertical: 10,
    width: '100%'
  },
  ButtonShared:{
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 70,
    paddingVertical: 5,
    marginHorizontal: 0
  }
});
