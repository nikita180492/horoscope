import React from 'react';
import {
  ImageBackground, ScrollView, StatusBar,
  Text,
  View,
} from 'react-native';
import Bg from '../assets/images/Bg.jpg';
import main from '../assets/styles/styles';

const AreaMain = ({children}) => {
  return (
    <ImageBackground source={Bg} resizeMode="cover" style={[main.LoginBg]}>
      <View style={[main.Overlay]}>
        {children}
      </View>
    </ImageBackground>
  );
};

export default AreaMain;
