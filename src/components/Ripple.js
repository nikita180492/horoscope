import React from 'react';
import RippleArea from 'react-native-material-ripple';

const Ripple = ({children,style,callback}) => {
  return (
    <RippleArea
      rippleColor={'#ccc'}
      rippleSize={176}
      rippleDuration={600}
      activeOpacity={0.7}
      style={style}
      onPress={callback}
    >
      {children}
    </RippleArea>

  );
};

export default Ripple;
