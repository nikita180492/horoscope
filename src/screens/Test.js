import React from 'react';
import {
  Button,
  Text,
  View,
} from 'react-native';
import {useAnimatedStyle, useSharedValue} from 'react-native-reanimated';

const Test = () => {
   const offset = useSharedValue(0);

  const animatedStyles = useAnimatedStyle(() => {
    return {
      transform: [{ translateX: offset.value * 100 }],
    };
  });

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Test</Text>

      <Animated.View style={[{width: 100, height: 100, backgroundColor: 'red'}, animatedStyles]} />

      <Button
        onPress={() => {
          offset.value = withSpring(Math.random());
        }}
        title="Move"
      />

    </View>
  );
};

export default Test;
