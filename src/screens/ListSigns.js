import React, {useEffect,useState, useCallback} from 'react';
import {
  ScrollView,
  Text,
  View,
  Pressable,
  BackHandler,
  ActivityIndicator
} from 'react-native';
import main from '../assets/styles/styles';
import AreaMain from '../components/AreaMain';
import {Signs} from '../library/utils/utils';
import RippleArea from '../components/Ripple';
import * as Animatable from 'react-native-animatable';
import {useFocusEffect} from '@react-navigation/native';
import {Color} from '../library/utils/colors';

const AnimatableTouchableOpacity =
  Animatable.createAnimatableComponent(Pressable);

const ListSigns = ({navigation}) => {

  const [loading,setLoading] = useState(true)

  useFocusEffect(
    useCallback(() => {
      setLoading(false)
      return () => {
        setLoading(true)
      };
    }, []),
  );

  useEffect(()=>{
    return ()=>{
      BackHandler.exitApp()
    }
  },[])

  const animation = {
    from: {
      opacity: 0,
      translateY: 60,
    },
    to: {
      opacity: 1,
      translateY: 0,
    },
  };

  if (loading) {
    return (
      <AreaMain>
        <View style={[{justifyContent: 'center', alignItems: 'center', flex: 1}]}>
          <ActivityIndicator size='large' color={Color.color5}/>
        </View>
      </AreaMain>
    );
  }

  return (
    <AreaMain>
      <ScrollView contentContainerStyle={[main.ScrollContainer]}>
        <View style={[main.Container]}>

          <View style={[main.SignsList]}>
            {Signs?.map((sign, index) => {
              return (
                <AnimatableTouchableOpacity
                  useNativeDriver
                  animation={animation}
                  delay={150 + (index  + 1) * 50}
                  key={sign.id}
                  style={[main.SignsBlock]}
                >
                  <RippleArea
                    style={[main.SignsBlockRipple]}
                    callback={() => navigation.navigate('Sign', {sign: sign.title})}>
                    <View style={[main.SignsBlockImage]}>
                      {sign.image}
                    </View>
                    <Text style={[main.Text, main.SignsBlockText]}>{sign.title}</Text>
                  </RippleArea>
                </AnimatableTouchableOpacity>
              );
            })}
          </View>
        </View>
      </ScrollView>
    </AreaMain>
  );
};

export default ListSigns;
