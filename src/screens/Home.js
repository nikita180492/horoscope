import React from 'react';
import {
  ScrollView,
  Text,
  View,
} from 'react-native';
import main from '../assets/styles/styles';
import AreaMain from '../components/AreaMain';

const Home = () => {
  return (
    <ScrollView contentContainerStyle={[main.ScrollContainer]}>
      <AreaMain>

      </AreaMain>
    </ScrollView>
  );
};

export default Home;
