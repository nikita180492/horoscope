import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  Text,
  View,
  Pressable,
  ActivityIndicator,
  Button, Linking, Share,
} from 'react-native';
import AreaMain from '../components/AreaMain';
import api from '../api';
import main from '../assets/styles/styles';
import {Color} from '../library/utils/colors';
import {receiveSvg} from '../library/utils/utils';
import SharedImage from '../assets/images/user-shared.svg';
import RippleArea from '../components/Ripple';
import * as Animatable from 'react-native-animatable';


const AnimatableView =
  Animatable.createAnimatableComponent(View)


const Sign = (props) => {

  const [loading, setLoading] = useState(true);
  const [dataSign, setSign] = useState({});
  const [day, setDay] = useState(['Today', 'Yesterday', 'Tomorrow']);
  const [selectDay, setSelectDay] = useState(day[0]);
  const [reloadSingBlock, setReloadSingBlock] = useState(true);

  let {sign} = props.route.params;

  const init = async () => {
    const data = {
      sign: sign,
      day: selectDay,
    };
    await api.HoroscopeInformation(data).then((res) => {
      setSign( JSON.parse(res.response));
      setLoading(false);
    }).catch(e => {
      console.log('e', e);
    });
  };

  useEffect(() => {
    init();
  }, []);

  useEffect(() => {
    setReloadSingBlock(true);
    init().then(() => {
      setReloadSingBlock(false);
    });
  }, [selectDay]);

  const messageShare = `
${sign} - ${dataSign.current_date}

Mood - ${dataSign.mood}
Color - ${dataSign.color}
Lucky number - ${dataSign.lucky_number}
Lucky time - ${dataSign.lucky_time}
Compatibility - ${dataSign.compatibility}

Description
${dataSign.description}

Download app - https://test.com
  `;

  const onShare = async () => {
    try {
      const result = await Share.share({
        title: 'sadsad',
        message: messageShare,
      }, {
        dialogTitle: 'Send a horoscope',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  if (loading) {
    return (
      <AreaMain>
        <View style={[{justifyContent: 'center', alignItems: 'center', flex: 1}]}>
          <ActivityIndicator size='large' color={Color.color5}/>
        </View>
      </AreaMain>
    );
  }

  const animation = {
    from: {
      opacity: 0,
      translateY: 60,
    },
    to: {
      opacity: 1,
      translateY: 0,
    },
  };
  return (
    <AreaMain>
      <ScrollView contentContainerStyle={[main.ScrollContainer]}>

        <View style={[main.Container]}>

          <Text style={[main.H1]}>
            {sign}
          </Text>


          <AnimatableView
            useNativeDriver
            animation={animation}
            delay={150 + (1 + 1) * 10}
            style={[main.Sign]}>
            {reloadSingBlock
              ?
              <View style={[{justifyContent: 'center', alignItems: 'center', flex: 1}]}>
                <ActivityIndicator size='large' color={Color.color5}/>
              </View>
              :
              <>
                <View style={[main.SignImage]}>
                  {receiveSvg(sign)}
                </View>
                <View style={[main.SignInfo]}>

                  <Text style={[main.H3, main.SignInfoText]}>
                    {dataSign.current_date}
                  </Text>
                  <Text style={[main.H4, main.SignInfoText]}>
                    {dataSign.date_range}
                  </Text>
                </View>
              </>
            }
          </AnimatableView>

          <AnimatableView
            useNativeDriver
            animation={animation}
            delay={150 + (2 + 1) * 20}
            style={[main.Sign]}>
            {reloadSingBlock
              ?
              <View style={[{justifyContent: 'center', alignItems: 'center', flex: 1}]}>
                <ActivityIndicator size='large' color={Color.color5}/>
              </View>
              :
              <View style={[]}>
                <View style={[main.SignInfo, {flexDirection: 'row'}]}>

                  <View style={[main.SignInfoCol]}>
                    <Text style={[main.H4, main.SignInfoText]}>
                      Mood - <Text style={[main.Text]}>{dataSign.mood}</Text>
                    </Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Text style={[main.H4, main.SignInfoText]}>Color - </Text>

                      <Pressable
                        onPress={() => {
                          Linking.openURL(`https://rgbcolorcode.com/color/${dataSign.color}`);
                        }}
                        style={[main.TextColor]}>
                        <Text style={[main.Text, {textDecorationLine: 'underline'}]}>{dataSign.color}</Text>
                      </Pressable>
                    </View>
                  </View>

                  <View style={[main.SignInfoCol]}>
                    <Text style={[main.H4, main.SignInfoText]}>
                      Lucky number - <Text style={[main.Text]}>{dataSign.lucky_number}</Text>
                    </Text>
                    <Text style={[main.H4, main.SignInfoText]}>
                      Lucky time - <Text style={[main.Text]}>{dataSign.lucky_time}</Text>
                    </Text>
                  </View>
                </View>

                <Text style={[main.H4, main.SignInfoText, {textAlign: 'center'}]}>
                  Compatibility - <Text style={[main.Text]}>{dataSign.compatibility} </Text>
                  <View style={{
                    width: 25,
                    height: 25,
                    transform: [{scale: .5}],
                  }}>
                    {receiveSvg(dataSign.compatibility)}
                  </View>
                </Text>

              </View>

            }

          </AnimatableView>

          <AnimatableView
            useNativeDriver
            animation={animation}
            delay={150 + (3 + 1) * 30}
            style={[main.SignButtonsArea]}>
            <RippleArea
              callback={() => setSelectDay(day[1])}
              style={[main.SignButton, selectDay === day[1] ? main.SignButtonActive : '']}>
              <Text style={[main.SignButtonText]}>
                {day[1]}
              </Text>
            </RippleArea>

            <RippleArea
              callback={() => setSelectDay(day[0])}
              style={[main.SignButton, selectDay === day[0] ? main.SignButtonActive : '']}>
              <Text style={[main.SignButtonText]}>
                {day[0]}
              </Text>
            </RippleArea>


            <RippleArea
              callback={() => setSelectDay(day[2])}
              style={[main.SignButton, selectDay === day[2] ? main.SignButtonActive : '']}>
              <Text style={[main.SignButtonText]}>
                {day[2]}
              </Text>
            </RippleArea>

          </AnimatableView>

          <AnimatableView
            useNativeDriver
            animation={animation}
            delay={150 + (4 + 1) * 40}
            style={[main.SignDescription]}>

            <View
              style={[{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap'}]}>
              <Text style={[main.H2]}>
                Description
              </Text>
              <RippleArea
                callback={() => onShare()}
                style={[main.SignButton, main.ButtonShared]}>
                <SharedImage width={15} height={15}/>
              </RippleArea>
            </View>

            <View style={[main.Sign]}>
              {reloadSingBlock
                ?
                <View style={[{justifyContent: 'center', alignItems: 'center', flex: 1}]}>
                  <ActivityIndicator size='large' color={Color.color5}/>
                </View>
                :
                <Text style={[main.Text, main.SignDescriptionText]}>
                  {dataSign.description}
                </Text>
              }

            </View>
          </AnimatableView>

        </View>

      </ScrollView>
    </AreaMain>
  );
};

export default Sign;
