import React, {useEffect,useState,useCallback} from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet, Pressable, ActivityIndicator,
} from 'react-native';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import main from '../assets/styles/styles';
import AreaMain from '../components/AreaMain';
import RippleArea from '../components/Ripple';
import * as Animatable from 'react-native-animatable';
;
const AnimatableView =
  Animatable.createAnimatableComponent(View)

const AnimatableButton =
  Animatable.createAnimatableComponent(Pressable)

const Start = ({navigation}) => {

  const [loading,setLoading] = useState(true)

  const animationText = {
    from: {
      opacity: 0,
      translateY: -60,
    },
    to: {
      opacity: 1,
      translateY: 0,
    },
  };

  const animationButton = {
    from: {
      opacity: 0,
      translateY: 60,
    },
    to: {
      opacity: 1,
      translateY: 0,
    },
  };

  useFocusEffect(
    useCallback(() => {
      setLoading(false)
      return () => {
        setLoading(true)
      };
    }, []),
  );

  if (loading) {
    return (
      <AreaMain>
        <View style={[{justifyContent: 'center', alignItems: 'center', flex: 1}]}>
          {/*<ActivityIndicator size='large' color={Color.color5}/>*/}
        </View>
      </AreaMain>
    );
  }

  return (
    <AreaMain>
    <ScrollView contentContainerStyle={[main.ScrollContainer]}>

        <View style={[main.Container, {justifyContent: 'center', flex: 1}]}>
          <View style={[styles.StartTextArea]}>
            <AnimatableView
              style={{alignItems: 'center'}}
              useNativeDriver
              animation={animationText}
              delay={150 + (1 + 1) * 50}>
              <Text
                style={[main.H1]}>
                Thank you!
              </Text>
              <Text style={[main.H3, {textAlign: 'center'}]}>
                Enjoy using the app!
              </Text>

            </AnimatableView>
            <AnimatableButton
              style={[main.GoToInApp]}
              useNativeDriver
              animation={animationButton}
              delay={150 + (1 + 1) * 50}
            >
              <RippleArea
                style={[main.GoToInAppRipple]}
                callback={() => navigation.navigate('ListSigns')}>
                <Text style={[main.H4, {marginBottom: 0}]}>Go to in app</Text>
              </RippleArea>
            </AnimatableButton>

          </View>
        </View>

    </ScrollView>
    </AreaMain>
  );
};
const styles = StyleSheet.create({
  StartTextArea:{
    alignItems: 'center',
    textAlign: 'center'
  }
})
export default Start;
