import React,{useEffect} from 'react';
import type {Node} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Start from './src/screens/Start';
import Home from './src/screens/Home';
import ListSigns from './src/screens/ListSigns';
import Sign from './src/screens/Sign';
import Test from './src/screens/Test';
import AreaMain from './src/components/AreaMain';

const Stack = createNativeStackNavigator();

const App: () => Node = () => {

  return (
    <AreaMain>

      <NavigationContainer>
        <Stack.Navigator initialRouteName="Start"
                         screenOptions={{
                           headerShown: false,
                         }}>
          <Stack.Screen name="Start" component={Start}/>
          <Stack.Screen name="Home" component={Home}/>
          <Stack.Screen name="ListSigns" component={ListSigns}/>
          <Stack.Screen name="Sign" component={Sign}/>
          <Stack.Screen name="Test" component={Test}/>
        </Stack.Navigator>
      </NavigationContainer>
    </AreaMain>
  );
};


export default App;
